<?php

namespace Models;

use Core\Error;

class CommonModel extends \Core\Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function deleteAgentByMob($mobile){
        try{
            $this->db->delete('agentdetail',array('mobile'=>$mobile));
        } catch (PDOException $ex) {
            echo Error::display($ex);
        }
    }
}