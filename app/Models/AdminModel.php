<?php

namespace Models;

use Core\Error;

class AdminModel extends \Core\Model {

    public function __construct() {
        parent::__construct();
    }

    public function addAgent($data) {
        try {
            $this->db->insert('agentdetail', $data);
            return "Successfully registered!!";
        } catch (PDOException $ex) {
            echo Error::display("Registration Failed!" . $ex);
        }
    }
    
    public function getAgent(){
        return $this->db->select('SELECT * FROM agentdetail');
    }
    
      public function getPassword($name) {
        $data = $this->db->select("SELECT * FROM adminlogin WHERE username=:name", array(':name' => $name));
        return $data[0]->password;
    }

    public function addNewPassword($name,$pass){
        $this->db->update('adminlogin',array('password'=>$pass),array('username',$name));
    }
}
