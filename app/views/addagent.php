<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">ADD AGENTS</a>
            </li>

        </ol>
        <!-- Icon Cards-->

        <div class="col-sm-1 col-sm-1 mb-3">

        </div>
    </div>

    <!-- register form -->
    <div class="container">

        <div class="card card-register mx-4 ">
            <div class="card-header">Register new agent</div>
            <div class="card-body">
                <form method="post">
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-7">
                                <label for="exampleInputName"> Name</label>
                                <input class="form-control" id="exampleInputName" type="text" aria-describedby="nameHelp" placeholder="Enter name" name="name" required="">
                            </div>
                            <div class="col-md-7">
                                <label for="exampleInputLastName">Address</label>
                                <textarea  class="form-control" id="exampleInputAddress"  aria-describedby="nameHelp" placeholder="Enter Address"  rows="3" cols="50" name="address"></textarea>
                            </div>

                            <div class= "col-md-7">
                                <label for="exampleInputEmail1">Mobile</label>
                                <input class="form-control" id="exampleInputPhonenumber" type="text" aria-describedby="mobileHelp" pattern="^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$" placeholder="Enter Mobile Number" name="mobile" required="">
                            </div>
                        </div>  
                    </div>

                    <div class="form-group">
                        <div class="form-row">

                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block col-md-7" name="save">Register</button>
                </form>

            </div>
        </div>
    </div>

    <!--table-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i>&nbspAGENT LIST</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

                    <thead>    
                        <tr>
                            <th>Username</th>
                            <th>Mobile</th>
                            <th>Address</th>
                            <th></th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php
                        foreach ($data['details'] as $dat) {
                            echo '<tr>';
                            echo '<td>'.$dat->name.'</td>';
                            echo '<td>'.$dat->mobile.'<input type="text" class="mobile" value="'.$dat->mobile.'" hidden ></td>';
                            echo '<td>'.$dat->address.'</td>';
                            echo '<td><button class="btn btn-danger delete">remove</button></td>';
                            echo '</tr>';
                        }
                        ?>        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
  $(document).ready(function () {
        $('.delete').click(function () {
            var tr = $(this).parent().parent();
             console.log(tr.find('input').val());
            if (confirm('Are you sure ?')) {
                $.post('deleteagent', {'mobile': tr.find('.mobile').val()}, function (data) {
                    tr.remove();
                   
                });
            }
        });
    });

    

</script>