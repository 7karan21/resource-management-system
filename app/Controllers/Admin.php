<?php

namespace Controllers;

use Core\View,
    Helpers\Session,
    Helpers\Url;

class Admin extends \Core\Controller {

    private $_model;

    public function __construct() {
        parent::__construct();
        if (!Session::get('rmsadmin')) {
            Url::redirect();
        }
        $this->_model = new \Models\AdminModel();
    }

    public function loadHeader($title) {
        View::renderTemplate('header', array('title' => $title));
        View::render('common/head');
    }

    public function loadFooter() {
        View::render('common/foot');
        View::renderTemplate('footer');
    }

    public function home() {
        $this->loadHeader('Home');
        View::render('home');
        $this->loadFooter();
    }

    public function addAgent() {
        $this->loadHeader('Add Agent');
        if (isset($_POST['save'])) {
            $data = array(
                'name' => $_POST['name'],
                'address' => $_POST['address'],
                'mobile' => $_POST['mobile']
            );
            $success = $this->_model->addAgent($data);
            $login = array(
                'username' => '',
                'password' => '',
                'userid' => $_POST['mobile']
            );
        }
        $details = $this->_model->getAgent();
        View::render('addagent', array('details' => $details));
        $this->loadFooter();
    }

    public function changePassword() {
        $this->loadHeader('Change Password');
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            if (password_verify($_POST['old'], $this->_model->getPassword($name))) {
                if ($_POST['new'] == $_POST['cnew']) {
                    $options = [
                        'cost' => 11
                    ];
                    $hash = password_hash($_POST['new'], PASSWORD_BCRYPT, $options);
                    $this->_model->addNewPassword($name,$hash);
                    $data = "Password updated successfully!";
                } else {
                    $error[] = "New password missmatched with confirmation!!";
                }
            } else {
                $error[] = "Wrong password!!";
            }
        }
        View::render('changepassword', $data, $error);
        $this->loadFooter();
    }

}
