<?php

namespace Controllers;

use Core\View,
    Core\Error,
    Helpers\Session,
    Helpers\Url;

class Common extends \Core\Controller {

    private $_model;

    public function __construct() {
        parent::__construct();
        if (!Session::get('rmsadmin')) {
            Url::redirect();
        }
        $this->_model = new \Models\CommonModel();
    }

    public function deleteAgent() {
        if(isset($_POST['mobile'])){
            $this->_model->deleteAgentByMob($_POST['mobile']);
            echo $_POST['mobile'];
        }
    }

}
